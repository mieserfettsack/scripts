javascript: (function() {
    var startPriceInRef = 6,
        maxAmountOfItems = 38;

    function shuffle(array) {
        var m = array.length,
            t, i;
        while (m) {
            i = Math.floor(Math.random() * m--);
            t = array[m];
            array[m] = array[i];
            array[i] = t
        }
        return array
    }
    for (var i = 59; i >= startPriceInRef; i--) {
        if (startPriceInRef != i) {
            maxAmountOfItems = maxAmountOfItems - shuffle($('*[data-item-value=' + i + ']')).length;
            console.log(shuffle($('*[data-item-value=' + i + ']')).length)
        }
        if (maxAmountOfItems > 0) {
            shuffle($('*[data-item-value=' + i + ']')).slice(0, maxAmountOfItems).trigger('click')
        }
    }
})()
