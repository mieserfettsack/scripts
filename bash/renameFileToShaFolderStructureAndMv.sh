#!/bin/bash

DSM_IMAGE_REGEX_FIND=".*\.\(webp\|png\|tiff\|tif\|jpg\|gif\|jpeg\|jp2\|bmp\|cr2\|dng\)"
DSM_VIDEO_REGEX_FIND=".*\.\(h264\|ogv\|webm\|3gp\|mpg\|mov\|avi\|mp4\|flv\|mpeg\|mkv\|vob\|drc\|qt\|wmv\|yuv\|rm\|rmvb\|asf\|m4v\|mpe\|mpv\|3g2\|f4v\)"
DSM_AUDIO_REGEX_FIND=".*\.\(m4a\|aac\|amr\|mp3\|flac\|opus\)"
DSM_MISC_REGEX_FIND=".*\.\(xcf\|pdf\|psd\)"
DSM_MV_FILES=1
DSM_SAVE_DUPLICATES=0
DSM_DEBUG_OUTPUT=0
DSM_FOLDER_DEPTH=2

# called inside _allFilesToStorage
# Deals with a new file which is about to be moved or copied to its new storage folder.
# New files are generated this way: storage/images/F/F/F/F/F/FFFFF1E...A71.jpg
# Parameters: DSM_ORIGINAL_FILE, DSM_STORAGE_FOLDER, DSM_STORAGE_TYPE
# All parameters are required!
# Example with parameters: _singleFileToStorage "/complete/path/to/the/file.jpg" "storage" "images"
function _singleFileToStorage () {
    DSM_ORIGINAL_FILE=""
    DSM_STORAGE_FOLDER=""
    DSM_STORAGE_TYPE=""

    if [ $# -ge 1 ]; then
        DSM_ORIGINAL_FILE=$1
    else
        if [ "$DSM_DEBUG_OUTPUT" -eq "1" ]; then
            echo "Ups! _singleFileToStorage() was called without DSM_ORIGINAL_FILE!"
        fi
    fi

    if [ $# -ge 2 ]; then
        DSM_STORAGE_FOLDER=$2
    else
        if [ "$DSM_DEBUG_OUTPUT" -eq "1" ]; then
            echo "Ups! _singleFileToStorage() was called without DSM_STORAGE_FOLDER!"
        fi
    fi

    if [ $# -ge 3 ]; then
        DSM_STORAGE_TYPE=$3
    else
        if [ "$DSM_DEBUG_OUTPUT" -eq "1" ]; then
            echo "Ups! _singleFileToStorage() was called without DSM_STORAGE_TYPE!"
        fi
    fi

    if [ $# -ge 1 -a $# -ge 2 -a $# -ge 3 ]; then
        mkdir -p "$DSM_STORAGE_FOLDER"
        DSM_DUPLICATE_FOLDER="$DSM_STORAGE_FOLDER/duplicates"
        DSM_FILE_NAME=$(basename "$DSM_ORIGINAL_FILE")
        DSM_FILE_UID=`ls -i "$DSM_ORIGINAL_FILE" | awk '{print $1}'`
        DSM_FILE_EXTENSION_RAW="${DSM_FILE_NAME##*.}"
        DSM_FILE_EXTENSION_LOWER=`echo "$DSM_FILE_EXTENSION_RAW" | awk '{print tolower($0)}'`

        DSM_FILE_HASH_COMPLETE=`sha256sum "$DSM_ORIGINAL_FILE" | awk '{print toupper($1)}'`
        DSM_FILE_HASH_1=`echo "$DSM_FILE_HASH_COMPLETE" | head -c 1 | tail -c 1`
        DSM_FILE_HASH_2=`echo "$DSM_FILE_HASH_COMPLETE" | head -c 2 | tail -c 1`
        DSM_FILE_HASH_3=`echo "$DSM_FILE_HASH_COMPLETE" | head -c 3 | tail -c 1`
        DSM_FILE_HASH_4=`echo "$DSM_FILE_HASH_COMPLETE" | head -c 4 | tail -c 1`
        DSM_FILE_HASH_5=`echo "$DSM_FILE_HASH_COMPLETE" | head -c 5 | tail -c 1`
        DSM_FILE_HASH_6=`echo "$DSM_FILE_HASH_COMPLETE" | head -c 6 | tail -c 1`
        DSM_FILE_HASH_7=`echo "$DSM_FILE_HASH_COMPLETE" | head -c 7 | tail -c 1`

        case "$DSM_FOLDER_DEPTH" in
            "1")
                DSM_NEW_FOLDER="$DSM_STORAGE_FOLDER/$DSM_STORAGE_TYPE/$DSM_FILE_HASH_1"
            ;;
            "2")
                DSM_NEW_FOLDER="$DSM_STORAGE_FOLDER/$DSM_STORAGE_TYPE/$DSM_FILE_HASH_1/$DSM_FILE_HASH_2"
            ;;
            "4")
                DSM_NEW_FOLDER="$DSM_STORAGE_FOLDER/$DSM_STORAGE_TYPE/$DSM_FILE_HASH_1/$DSM_FILE_HASH_2/$DSM_FILE_HASH_3/$DSM_FILE_HASH_4"
            ;;
            "5")
                DSM_NEW_FOLDER="$DSM_STORAGE_FOLDER/$DSM_STORAGE_TYPE/$DSM_FILE_HASH_1/$DSM_FILE_HASH_2/$DSM_FILE_HASH_3/$DSM_FILE_HASH_4/$DSM_FILE_HASH_5"
            ;;
            "6")
                DSM_NEW_FOLDER="$DSM_STORAGE_FOLDER/$DSM_STORAGE_TYPE/$DSM_FILE_HASH_1/$DSM_FILE_HASH_2/$DSM_FILE_HASH_3/$DSM_FILE_HASH_4/$DSM_FILE_HASH_5/$DSM_FILE_HASH_6"
            ;;
            "7")
                DSM_NEW_FOLDER="$DSM_STORAGE_FOLDER/$DSM_STORAGE_TYPE/$DSM_FILE_HASH_1/$DSM_FILE_HASH_2/$DSM_FILE_HASH_3/$DSM_FILE_HASH_4/$DSM_FILE_HASH_5/$DSM_FILE_HASH_6/$DSM_FILE_HASH_6"
            ;;
            *)
                DSM_NEW_FOLDER="$DSM_STORAGE_FOLDER/$DSM_STORAGE_TYPE/$DSM_FILE_HASH_1/$DSM_FILE_HASH_2/$DSM_FILE_HASH_3"
            ;;
        esac

        DSM_NEW_FILE="$DSM_NEW_FOLDER/$DSM_FILE_HASH_COMPLETE.$DSM_FILE_EXTENSION_LOWER"

        if [ "$DSM_DEBUG_OUTPUT" -eq "1" ]; then
            echo "Type: $DSM_STORAGE_TYPE"
            echo "This file: $DSM_ORIGINAL_FILE (inode_uid: $DSM_FILE_UID)"
            if [ "$DSM_MV_FILES" -eq "1" ]; then
                echo "Will be moved to: $DSM_NEW_FILE"
            else
                echo "Will be copied to: $DSM_NEW_FILE"
            fi
        fi

        mkdir -p "$DSM_NEW_FOLDER"

        if [ -f "$DSM_NEW_FILE" ]; then
            mkdir -p "$DSM_DUPLICATE_FOLDER"
            if [ "$DSM_MV_FILES" -eq "1" -a "$DSM_SAVE_DUPLICATES" -eq "1" ]; then
                mv "$DSM_ORIGINAL_FILE" "$DSM_DUPLICATE_FOLDER"/"$DSM_FILE_HASH_COMPLETE"_"$DSM_FILE_UID"."$DSM_FILE_EXTENSION_LOWER"
            else
                cp -fu "$DSM_ORIGINAL_FILE" "$DSM_DUPLICATE_FOLDER"/"$DSM_FILE_HASH_COMPLETE"_"$DSM_FILE_UID"."$DSM_FILE_EXTENSION_LOWER"
            fi
        else
            if [ "$DSM_MV_FILES" -eq "1" ]; then
                mv "$DSM_ORIGINAL_FILE" "$DSM_NEW_FILE"
            else
                cp -fu "$DSM_ORIGINAL_FILE" "$DSM_NEW_FILE"
            fi
        fi
    else
        echo "ERROR!"
    fi
}

# function to copy or move files to sha512 storage folder structure
function _allFilesToStorage () {
    DSM_NEW_FILES_FOLDER=""
    DSM_STORAGE_FOLDER=""

    if [ $# -ge 1 ]; then
        DSM_NEW_FILES_FOLDER=$1
    else
        echo "Error: Please provide a search-folder, where your files are located."
    fi

    if [ $# -ge 2 ]; then
        DSM_STORAGE_FOLDER=$2
    else
        echo "Error: Please provide a storage-folder, where your files will be moved to."
    fi

    if [ $# -ge 1 -a $# -ge 2 ]; then
        if [ "$DSM_NEW_FILES_FOLDER" == "/" ]; then
            echo "Error: This script should not be run on /."
        else
            if [[ -n "$DSM_NEW_FILES_FOLDER" ]]; then
                find "${DSM_NEW_FILES_FOLDER%%+(/)}" -not -empty -type f -iregex ${DSM_IMAGE_REGEX_FIND} -prune | while read DSM_ORIGINAL_FILE; do
                    _singleFileToStorage "$DSM_ORIGINAL_FILE" "${DSM_STORAGE_FOLDER%%+(/)}" "images"
                done

                find "${DSM_NEW_FILES_FOLDER%%+(/)}" -not -empty -type f -iregex ${DSM_VIDEO_REGEX_FIND} -prune | while read DSM_ORIGINAL_FILE; do
                    _singleFileToStorage "$DSM_ORIGINAL_FILE" "${DSM_STORAGE_FOLDER%%+(/)}" "videos"
                done

                find "${DSM_NEW_FILES_FOLDER%%+(/)}" -not -empty -type f -iregex ${DSM_AUDIO_REGEX_FIND} -prune | while read DSM_ORIGINAL_FILE; do
                    _singleFileToStorage "$DSM_ORIGINAL_FILE" "${DSM_STORAGE_FOLDER%%+(/)}" "audios"
                done

                find "${DSM_NEW_FILES_FOLDER%%+(/)}" -not -empty -type f -iregex ${DSM_MISC_REGEX_FIND} -prune | while read DSM_ORIGINAL_FILE; do
                    _singleFileToStorage "$DSM_ORIGINAL_FILE" "${DSM_STORAGE_FOLDER%%+(/)}" "misc"
                done
            fi
        fi
    fi
}
